function toggle_menu(data) {
    if((window.event.toElement.id).length || data){
        document.getElementsByTagName('body')[0].classList.toggle('fix_overflow');
        document.getElementById('mobile_menu').classList.toggle('active');
    }
}
function toggle_search() {
    document.getElementsByClassName('input_search_area')[0].classList.toggle('show');
    document.getElementsByClassName('input_search_area')[0].getElementsByTagName('input')[0].focus();

}


function height_window_to_main(flex = true) {
    var header = document.getElementsByTagName('header')[0];
    var main = document.getElementsByTagName('main')[0];
    main.style.minHeight = window.innerHeight - header.offsetHeight + 'px';
    if(flex == "stretch")
    {
        main.style.display = 'flex';
        main.style.alignItems = 'stretch';
    }
    else if(flex) {
        main.style.display = 'flex';
        main.style.alignItems = 'center';
    }
}