//slider
function Slider(slider_id,full = true){

    var self = this;

    var init = false;

    var dom_slider;
    var dom_window;
    var dom_carousel;
    var dom_sliders;
    var dom_controller;
    var dom_slide_links;

    var count_sliders = 0;
    var action_slider = 1;

    var height = 0;
    var width = 0;

    //Интервал перелистывания слайдов
    var interval = 5;

    var timer_carousel;

    function initialization() {

        dom_slider = document.getElementById(slider_id);
        if(dom_slider === undefined) return false;
        dom_window = dom_slider.getElementsByClassName('slider_window')[0];
        if(dom_window === undefined) return false;
        dom_carousel = dom_slider.getElementsByClassName('slider_carousel')[0];
        if(dom_carousel === undefined) return false;
        dom_sliders = dom_slider.getElementsByClassName('slider_item');
        if(dom_sliders.length == 0) return false;

        if(full) {
            //Установка ширины слайдера
            width = window.innerWidth;

            //Поиск максимальной высоты
            for (var i = 0; i < dom_sliders.length; i++) {
                if (dom_sliders[i].offsetHeight > height) height = dom_sliders[i].offsetHeight;
            }
        }
        else
        {
            var parent = dom_slider.parentElement;
            width = parent.offsetWidth;
            height = parent.offsetHeight;
        }

        // минут место под пагинатор
        height -= 30;

        //Установка ширины и высоты слайдам, карусели, окну в котором будет карусель
        for (var i = 0; i < dom_sliders.length; i++) {
            dom_sliders[i].style.width = width + 'px';
            dom_sliders[i].style.height = height + 'px';
        }
        dom_window.style.widows = width + 'px';
        dom_window.style.height = dom_carousel.style.height =  height + 'px';
        dom_carousel.style.width = dom_sliders.length * width + 'px';

        count_sliders = dom_sliders.length;

        addController();
        init = true;
        return true;
    }

    //Автоматическая и по выбору прокрутка карусели
    function carousel(number = false) {
        if(number){
            action_slider = +number;
        }
        else if(count_sliders == action_slider)
        {
            action_slider = 1;
        }
        else{
            action_slider++;
        }

        dom_controller.getElementsByClassName('active')[0].classList.remove('active');
        dom_slide_links[action_slider-1].classList.add('active');

        dom_carousel.style.left = ((action_slider - 1) * width * -1) + 'px';
        clearTimeout(timer_carousel);
        timer_carousel = setTimeout(carousel,interval*1000);
    }

    //Управление слайдером
    function addController() {
        var controller = document.createElement('div');
        controller.classList.add('slider_controller');
        for(var i=1; i <= count_sliders; i++){
            var slide_link = document.createElement('div');
            slide_link.classList.add('slide_link');
            slide_link.setAttribute('slide',''+i);
            if(i==1) slide_link.classList.add('active');
            slide_link.onclick = function(event){
                carousel(event.target.getAttribute('slide'));
            }
            controller.appendChild(slide_link);
        }
        dom_controller = controller;
        dom_slide_links = dom_controller.getElementsByClassName('slide_link');
        dom_slider.appendChild(controller);
    }

    //ЗАпуск слайдера
    this.start = function(){
        if(!init){
            if(!initialization()) return false;
        }
        setTimeout(carousel,interval*1000);
    }

}